//
//  RecentsTableViewController.h
//  EndQuote
//
//  Created by Josh Rael on 4/8/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REMenu.h"
#import "BaseQuotesViewController.h"

@interface RecentsTableViewController : BaseQuotesViewController

@end
