//
//  Quote.h
//  EndQuote
//
//  Created by Josh Rael on 4/14/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Quote : NSObject

@property (strong, nonatomic) NSString *quoteString;
@property (strong, nonatomic) NSString *authorString;
@property (strong, nonatomic) NSString *titleString;
@property int rowid;

- (id) initWithQuote:(NSString*)quote andAuthor:(NSString*)author andTitle:(NSString*)title andId:(int)rid;

@end
