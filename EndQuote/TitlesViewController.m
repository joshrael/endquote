//
//  TitlesViewController.m
//  EndQuote
//
//  Created by Josh Rael on 4/8/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import "TitlesViewController.h"
#import "NavigationViewController.h"
#import "BaseQuotesViewController.h"
#import "Database.h"

@interface TitlesViewController ()

@end

@implementation TitlesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(toggleMenu)]];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(presentAddView)]];
    
    [self setTitle:@"Titles"];
    
    self.quotes = [Database fetchAllTitles];
}

-(void)didDismissAddViewWithQuote:(NSDictionary *)quote
{
    [Database saveQuoteWithQuote:[quote objectForKey:@"quote"] andAuthor:[quote objectForKey:@"author"] andTitle:[quote objectForKey:@"title"]];
    self.quotes = [Database fetchAllTitles];
    
    [self.tableView reloadData];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSDictionary *temp = [self.quotes objectAtIndex:indexPath.row];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 280, 30)];
    [titleLabel setText:[temp objectForKey:@"title"]];
    [cell.contentView addSubview:titleLabel];
    
    UILabel *authorLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 30, 280, 30)];
    [authorLabel setText:[temp objectForKey:@"author"]];
    [cell.contentView addSubview:authorLabel];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *temp = [self.quotes objectAtIndex:indexPath.row];
    NSString *tempTitle = [temp objectForKey:@"title"];
    
    BaseQuotesViewController *quotesByTitleView = [[BaseQuotesViewController alloc] init];
    
    [quotesByTitleView setQuotes:[Database fetchAllQuotesByTitle:tempTitle]];
    [quotesByTitleView setTitle:tempTitle];
    
    [self.navigationController pushViewController:quotesByTitleView animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
