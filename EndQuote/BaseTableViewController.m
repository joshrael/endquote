//
//  BaseTableViewController.m
//  EndQuote
//
//  Created by Josh Rael on 4/8/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import "BaseTableViewController.h"
#import "NavigationViewController.h"
#import "Database.h"


@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
}

- (void)presentAddView
{
    AddQuoteViewController *addController = [[AddQuoteViewController alloc] init];
    
    addController.delegate = self;
    
    UINavigationController *addNavigation = [[UINavigationController alloc] initWithRootViewController:addController];
    
    [self presentViewController:addNavigation animated:YES completion:NULL];
    
}

- (void)didDismissAddView
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)didDismissAddViewWithQuote:(NSDictionary *)quote
{
    [Database saveQuoteWithQuote:[quote objectForKey:@"quote"] andAuthor:[quote objectForKey:@"author"] andTitle:[quote objectForKey:@"title"]];
    self.quotes = [Database fetchAllQuotes];
    
    [self.tableView reloadData];
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.quotes count];
}






@end
