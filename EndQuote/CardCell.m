//
//  CardCell.m
//  EndQuote
//
//  Created by Josh Rael on 4/20/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import "CardCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation CardCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setupWithQuote:(NSString *)quote andAuthor:(NSString *)author andTitle:(NSString *)title
{
    self.quoteText = [[UITextView alloc]initWithFrame:CGRectMake(10, 20, 280, 100)];
    [self.quoteText setText:quote];
    [self.quoteText setTextAlignment:NSTextAlignmentCenter];
    [self.quoteText setFont:[UIFont fontWithName:@"Georgia" size:18]];
    [self.quoteText setEditable:NO];
    
    self.authorText = [[UILabel alloc] initWithFrame:CGRectMake(0, 130, 300, 30)];
    [self.authorText setText:author];
    [self.authorText setTextAlignment:NSTextAlignmentCenter];
    [self.authorText setFont:[UIFont fontWithName:@"Georgia" size:14]];
    
    
    self.titleText = [[UILabel alloc] initWithFrame:CGRectMake(0, 170, 300, 30)];
    [self.titleText setText:title];
    [self.titleText setTextAlignment:NSTextAlignmentCenter];
    [self.titleText setFont:[UIFont fontWithName:@"Georgia" size:14]];
    
    self.mainView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, 250)];
    self.mainView.layer.cornerRadius = 5;
    self.mainView.layer.masksToBounds =YES;
    
    [self.mainView addSubview:self.quoteText];
    [self.mainView addSubview:self.authorText];
    [self.mainView addSubview:self.titleText];
    
    [self.mainView setBackgroundColor:[UIColor whiteColor]];
    
    [self.contentView addSubview:self.mainView];
    
    [self setBackgroundColor:[UIColor clearColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
