//
//  CardCell.h
//  EndQuote
//
//  Created by Josh Rael on 4/20/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardCell : UITableViewCell

@property (strong, nonatomic) UIView *mainView;
@property (strong, nonatomic) UITextView *quoteText;
@property (strong, nonatomic) UILabel *authorText;
@property (strong, nonatomic) UILabel *titleText;

- (void) setupWithQuote:(NSString*)quote andAuthor:(NSString*) author andTitle:(NSString*) title;

@end
