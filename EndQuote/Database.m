//
//  Database.h
//  EndQuote
//
//  Created by Josh Rael on 4/19/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import "Database.h"

@implementation Database


static sqlite3 *db;

static sqlite3_stmt *createQuotes;
static sqlite3_stmt *fetchQuotes;
static sqlite3_stmt *fetchAuthors;
static sqlite3_stmt *fetchQuotesByAuthor;
static sqlite3_stmt *fetchTitles;
static sqlite3_stmt *fetchQuotesByTitle;
static sqlite3_stmt *insertQuote;
static sqlite3_stmt *deleteQuote;

+(void) createEditableCopyOfDatabaseIfNeeded
{
    BOOL success;
    
    // look for an existing photos database
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentDirectory stringByAppendingPathComponent:@"QuoteDB.sql"];
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success) return;
    
    // if failed to find one, copy the empty photos database into the location
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"QuoteDB.sql"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"FAILED to create writable database file with message, '%@'.", [error localizedDescription]);
    }
}

+(void) initDatabase
{
    // create the statement strings
    const char *createQuotesString = "CREATE TABLE IF NOT EXISTS quotes (rowid INTEGER PRIMARY KEY AUTOINCREMENT, quote TEXT, author TEXT, title TEXT)";
    const char *fetchQuotesString = "SELECT * FROM quotes";
    const char *fetchAuthorsString = "SELECT DISTINCT author FROM quotes";
    const char *fetchQuotesByAuthorString = "SELECT * FROM quotes WHERE author=?";
    const char *fetchTitlesString = "SELECT DISTINCT title, author FROM quotes";
    const char *fetchQuotesByTitleString = "SELECT * FROM quotes WHERE title=?";
    const char *insertQuoteString = "INSERT INTO quotes (quote, author, title) VALUES (?, ?, ?)";
    const char *deleteQuoteString = "DELETE FROM quotes WHERE rowid=?";
    
    // create the path to the database
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *path = [documentDirectory stringByAppendingPathComponent:@"QuoteDB.sql"];
    
    // open the database connection
    if (sqlite3_open([path UTF8String], &db)) {
        NSLog(@"ERROR opening the db");
    }
    
    int success;
    
    //init table statement
    if (sqlite3_prepare_v2(db, createQuotesString, -1, &createQuotes, NULL) != SQLITE_OK) {
        NSLog(@"Failed to prepare quotes create table statement");
    }
    
    // execute the table creation statement
    success = sqlite3_step(createQuotes);
    sqlite3_reset(createQuotes);
    if (success != SQLITE_DONE) {
        NSLog(@"ERROR: failed to create quotes table");
    }
    
    //init retrieval statement
    if (sqlite3_prepare_v2(db, fetchQuotesString, -1, &fetchQuotes, NULL) != SQLITE_OK) {
        NSLog(@"ERROR: failed to prepare quote fetching statement");
    }
    
    //init author retrieval statement
    if (sqlite3_prepare_v2(db, fetchAuthorsString, -1, &fetchAuthors, NULL) != SQLITE_OK) {
        NSLog(@"ERROR: failed to prepare author fetching statement");
    }
    
    //init quotes by author retrieval statement
    if (sqlite3_prepare_v2(db, fetchQuotesByAuthorString, -1, &fetchQuotesByAuthor, NULL) != SQLITE_OK) {
        NSLog(@"ERROR: failed to prepare quotes by author fetching statement");
    }
    
    //init title retrieval statement
    if (sqlite3_prepare_v2(db, fetchTitlesString, -1, &fetchTitles, NULL) != SQLITE_OK) {
        NSLog(@"ERROR: failed to prepare title fetching statement");
    }
    
    //init quotes by title retrieval statement
    if (sqlite3_prepare_v2(db, fetchQuotesByTitleString, -1, &fetchQuotesByTitle, NULL) != SQLITE_OK) {
        NSLog(@"ERROR: failed to prepare quotes by title fetching statement");
    }
    
    //init insertion statement
    if (sqlite3_prepare_v2(db, insertQuoteString, -1, &insertQuote, NULL) != SQLITE_OK) {
        NSLog(@"ERROR: failed to prepare quote inserting statement");
    }
    
    // init deletion statement
    if (sqlite3_prepare_v2(db, deleteQuoteString, -1, &deleteQuote, NULL) != SQLITE_OK) {
        NSLog(@"ERROR: failed to prepare quote deleting statement");
    }
}

+(NSMutableArray*) fetchAllQuotes
{
    NSMutableArray *ret = [NSMutableArray arrayWithCapacity:0];
    
    while (sqlite3_step(fetchQuotes) == SQLITE_ROW) {
        //query columns from fetch statement
        char *quoteChars = (char *) sqlite3_column_text(fetchQuotes, 1);
        char *authorChars = (char *) sqlite3_column_text(fetchQuotes, 2);
        char *titleChars = (char *) sqlite3_column_text(fetchQuotes, 3);
        
        // convert to NSStrings
        NSString *tempQuote = [NSString stringWithUTF8String:quoteChars];
        NSString *tempAuthor = [NSString stringWithUTF8String:authorChars];
        NSString *tempTitle = [NSString stringWithUTF8String:titleChars];
        
        //create Photo object
        Quote *temp = [[Quote alloc] initWithQuote:tempQuote andAuthor:tempAuthor andTitle:tempTitle andId:sqlite3_column_int(fetchQuotes, 0)];
        [ret addObject:temp];
    }
    
    sqlite3_reset(fetchQuotes);
    return ret;
}

+(NSMutableArray*) fetchAllAuthors
{
    NSMutableArray *ret = [NSMutableArray arrayWithCapacity:0];
    
    while (sqlite3_step(fetchAuthors) == SQLITE_ROW) {
        //query columns from fetch statement
        char *authorChars = (char *) sqlite3_column_text(fetchAuthors, 0);
        
        // convert to NSString
        NSString *tempAuthor = [NSString stringWithUTF8String:authorChars];
        [ret addObject:tempAuthor];
    }
    
    sqlite3_reset(fetchAuthors);
    return ret;
}

+(NSMutableArray*) fetchAllQuotesByAuthor:(NSString*)author
{
    NSMutableArray *ret = [NSMutableArray arrayWithCapacity:0];
    
    //bind data to the statement
    sqlite3_bind_text(fetchQuotesByAuthor, 1, [author UTF8String], -1, SQLITE_TRANSIENT);
    
    while (sqlite3_step(fetchQuotesByAuthor) == SQLITE_ROW) {
        //query columns from fetch statement
        char *quoteChars = (char *) sqlite3_column_text(fetchQuotesByAuthor, 1);
        char *authorChars = (char *) sqlite3_column_text(fetchQuotesByAuthor, 2);
        char *titleChars = (char *) sqlite3_column_text(fetchQuotesByAuthor, 3);
        
        // convert to NSStrings
        NSString *tempQuote = [NSString stringWithUTF8String:quoteChars];
        NSString *tempAuthor = [NSString stringWithUTF8String:authorChars];
        NSString *tempTitle = [NSString stringWithUTF8String:titleChars];
        
        //create Photo object
        Quote *temp = [[Quote alloc] initWithQuote:tempQuote andAuthor:tempAuthor andTitle:tempTitle andId:sqlite3_column_int(fetchQuotes, 0)];
        [ret addObject:temp];
    }
    
    sqlite3_reset(fetchQuotesByAuthor);
    return ret;
}
    
+(NSMutableArray*) fetchAllTitles
{
    NSMutableArray *ret = [NSMutableArray arrayWithCapacity:0];
    
    while (sqlite3_step(fetchTitles) == SQLITE_ROW) {
        //query columns from fetch statement
        char *titleChars = (char *) sqlite3_column_text(fetchTitles, 0);
        char *authorChars = (char *) sqlite3_column_text(fetchTitles, 1);
        
        
        // convert to NSString
        NSString *tempAuthor = [NSString stringWithUTF8String:authorChars];
        NSString *tempTitle = [NSString stringWithUTF8String:titleChars];
        NSDictionary *temp = @{@"author":tempAuthor, @"title":tempTitle};
        [ret addObject:temp];
    }
    
    sqlite3_reset(fetchTitles);
    return ret;
}

+(NSMutableArray*) fetchAllQuotesByTitle:(NSString*)title
{
    NSMutableArray *ret = [NSMutableArray arrayWithCapacity:0];
    
    //bind data to the statement
    sqlite3_bind_text(fetchQuotesByTitle, 1, [title UTF8String], -1, SQLITE_TRANSIENT);
    
    while (sqlite3_step(fetchQuotesByTitle) == SQLITE_ROW) {
        //query columns from fetch statement
        char *quoteChars = (char *) sqlite3_column_text(fetchQuotesByTitle, 1);
        char *authorChars = (char *) sqlite3_column_text(fetchQuotesByTitle, 2);
        char *titleChars = (char *) sqlite3_column_text(fetchQuotesByTitle, 3);
        
        // convert to NSStrings
        NSString *tempQuote = [NSString stringWithUTF8String:quoteChars];
        NSString *tempAuthor = [NSString stringWithUTF8String:authorChars];
        NSString *tempTitle = [NSString stringWithUTF8String:titleChars];
        
        //create Photo object
        Quote *temp = [[Quote alloc] initWithQuote:tempQuote andAuthor:tempAuthor andTitle:tempTitle andId:sqlite3_column_int(fetchQuotes, 0)];
        [ret addObject:temp];
    }
    
    sqlite3_reset(fetchQuotesByTitle);
    return ret;
}

+(void) saveQuoteWithQuote:(NSString *)quote andAuthor:(NSString *)author andTitle:(NSString *)title
{
    //bind data to the statement
    sqlite3_bind_text(insertQuote, 1, [quote UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(insertQuote, 2, [author UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(insertQuote, 3, [title UTF8String], -1, SQLITE_TRANSIENT);

    
    int success = sqlite3_step(insertQuote);
    sqlite3_reset(insertQuote);
    if (success != SQLITE_DONE) {
        NSLog(@"ERROR: Failed to insert quote");
    }
}

+(void) deleteQuote:(int)rowid
{
    //bind the row id, step the statement, reset the statement, check for error
    sqlite3_bind_int(deleteQuote, 1, rowid);
    int success = sqlite3_step(deleteQuote);
    sqlite3_reset(deleteQuote);
    if (success != SQLITE_DONE) {
        NSLog(@"ERROR: failed to delete the quote");
    }
}

+(void) cleanUpDatabaseForQuit
{
    sqlite3_finalize(fetchQuotes);
    sqlite3_finalize(fetchAuthors);
    sqlite3_finalize(fetchQuotesByAuthor);
    sqlite3_finalize(fetchTitles);
    sqlite3_finalize(fetchQuotesByTitle);
    sqlite3_finalize(insertQuote);
    sqlite3_finalize(deleteQuote);
    sqlite3_finalize(createQuotes);
    sqlite3_close(db);
}


@end
