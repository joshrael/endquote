//
//  NavigationViewController.m
//  EndQuote
//
//  Created by Josh Rael on 4/8/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import "NavigationViewController.h"
#import "RecentsTableViewController.h"
#import "AuthorsViewController.h"
#import "TitlesViewController.h"

@interface NavigationViewController ()

@property (strong, readwrite, nonatomic) REMenu *menu;

@end

@implementation NavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    __typeof (self) __weak weakSelf = self;
    REMenuItem *recentItem = [[REMenuItem alloc] initWithTitle:@"Recent"
                                                         image:[UIImage imageWithContentsOfFile:@""]
                                              highlightedImage:nil
                                                        action:^(REMenuItem *item) {
                                                            NSLog(@"item: %@", item);
                                                            RecentsTableViewController *controller = [[RecentsTableViewController alloc] init];
                                                            [weakSelf setViewControllers:@[controller] animated:NO];
                                                        }];
    recentItem.tag = 0;
    
    REMenuItem *authorItem = [[REMenuItem alloc] initWithTitle:@"Authors"
                                                         image:[UIImage imageWithContentsOfFile:@""]
                                              highlightedImage:nil
                                                        action:^(REMenuItem *item) {
                                                            NSLog(@"item: %@", item);
                                                            AuthorsViewController *controller = [[AuthorsViewController alloc] init];
                                                            [weakSelf setViewControllers:@[controller] animated:NO];
                                                        }];
    authorItem.tag = 1;
    
    REMenuItem *workItem = [[REMenuItem alloc] initWithTitle:@"Titles"
                                                         image:[UIImage imageWithContentsOfFile:@""]
                                              highlightedImage:nil
                                                        action:^(REMenuItem *item) {
                                                            NSLog(@"item: %@", item);
                                                            TitlesViewController *controller = [[TitlesViewController alloc] init];
                                                            [weakSelf setViewControllers:@[controller] animated:NO];
                                                        }];
    workItem.tag = 2;
    
    REMenuItem *tagItem = [[REMenuItem alloc] initWithTitle:@"Tags"
                                                       image:[UIImage imageWithContentsOfFile:@""]
                                            highlightedImage:nil
                                                      action:^(REMenuItem *item) {
                                                          NSLog(@"item: %@", item);
                                                      }];
    tagItem.tag = 3;
    
    self.menu = [[REMenu alloc] initWithItems:@[recentItem, authorItem, workItem]];
    [self.menu showFromNavigationController:self.navigationController];
    
    self.menu.appearsBehindNavigationBar = NO;
    
    //Styling the Menu
    self.menu.cornerRadius = 5.0;
    self.menu.backgroundColor = [UIColor colorWithHue:0.000 saturation:0.000 brightness:0.973 alpha:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)toggleMenu
{
    if (self.menu.isOpen)
        return [self.menu close];
    
    [self.menu showFromNavigationController:self];
}



@end
