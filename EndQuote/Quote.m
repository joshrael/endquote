//
//  Quote.m
//  EndQuote
//
//  Created by Josh Rael on 4/14/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import "Quote.h"

@implementation Quote

-(id)initWithQuote:(NSString *)quote andAuthor:(NSString *)author andTitle:(NSString *)title andId:(int)rid
{
    self = [super init];
    self.quoteString = quote;
    self.authorString = author;
    self.titleString = title;
    self.rowid = rid;
    return self;
}

@end
