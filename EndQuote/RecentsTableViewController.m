//
//  RecentsTableViewController.m
//  EndQuote
//
//  Created by Josh Rael on 4/8/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import "RecentsTableViewController.h"
#import "NavigationViewController.h"
#import "Database.h"

@interface RecentsTableViewController ()

@end

@implementation RecentsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(toggleMenu)]];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(presentAddView)]];

    [self setTitle:@"EndQuote"];
    
    
    self.quotes = [Database fetchAllQuotes];
}

-(void)didDismissAddViewWithQuote:(NSDictionary *)quote
{
    //    [self.quotes addObject:quote];
    [Database saveQuoteWithQuote:[quote objectForKey:@"quote"] andAuthor:[quote objectForKey:@"author"] andTitle:[quote objectForKey:@"title"]];
    self.quotes = [Database fetchAllQuotes];
    
    [self.tableView reloadData];
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



// support editing in the table view
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [Database deleteQuote:[[self.quotes objectAtIndex:indexPath.row] rowid]];
        self.quotes = [Database fetchAllQuotes];
        
        //[self.myImages removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        
    }
}


@end
