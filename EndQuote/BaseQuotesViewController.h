//
//  BaseQuotesViewController.h
//  EndQuote
//
//  Created by Josh Rael on 4/20/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseQuotesViewController : BaseTableViewController

@end
