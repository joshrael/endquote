//
//  AddQuoteViewController.h
//  EndQuote
//
//  Created by Josh Rael on 4/12/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol AddQuoteViewControllerDelegate <NSObject>

- (void) didDismissAddView;
- (void) didDismissAddViewWithQuote:(NSDictionary*)quote;

@end

@interface AddQuoteViewController : UIViewController 

@property (nonatomic, assign) id<AddQuoteViewControllerDelegate> delegate;

@property (strong, nonatomic) UIView* mainView;
@property (strong, nonatomic) UITextView* quoteField;
@property (strong, nonatomic) UITextField* authorField;
@property (strong, nonatomic) UITextField* titleField;

-(void) saveButtonPressed;
-(void) cancelButtonPressed;

- (CGSize)text:(NSString *)text sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size;

@end
