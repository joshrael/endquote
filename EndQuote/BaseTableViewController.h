//
//  BaseTableViewController.h
//  EndQuote
//
//  Created by Josh Rael on 4/8/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REMenu.h"
#import "AddQuoteViewController.h"
#import "Quote.h"

@interface BaseTableViewController : UITableViewController <AddQuoteViewControllerDelegate>

@property (strong, nonatomic) NSMutableArray *quotes;

- (void)presentAddView;

@end
