//
//  AddQuoteViewController.m
//  EndQuote
//
//  Created by Josh Rael on 4/12/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import "AddQuoteViewController.h"

@interface AddQuoteViewController ()

@end

@implementation AddQuoteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)]];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonPressed)]];
    [self setTitle:@"Add Quote"];
    
    
    
    self.quoteField = [[UITextView alloc] init];
    
    [self.quoteField setText:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vehicula mauris nisi, nec accumsan lacus euismod at. Ut euismod aliquet nisi eu pulvinar. Integer accumsan fringilla dolor, cursus consectetur leo sodales at. Maecenas dolor tellus"];
    [self.quoteField setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    [self.quoteField setContentInset:UIEdgeInsetsMake(-62, 0, 0, 0)];
    
    // Set height of text view according to the size of the quote:
    CGSize newSize = [self text:self.quoteField.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:CGSizeMake(280, 700)];
    [self.quoteField setFrame:CGRectMake(10, 10, 280, newSize.height+20)];
    
    self.quoteField.layer.borderColor = [UIColor redColor].CGColor;
    self.quoteField.layer.borderWidth = 1.0;
    
    
    
    self.authorField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10+newSize.height+20+20, 300, 30)];
    [self.authorField setPlaceholder:@"Author"];
    [self.authorField setTextAlignment:NSTextAlignmentCenter];
    
    
    self.titleField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10+newSize.height+20+20+30, 300, 30)];
    [self.titleField setPlaceholder:@"Title"];
    [self.titleField setTextAlignment:NSTextAlignmentCenter];
    
    
    self.mainView = [[UIView alloc] initWithFrame:CGRectMake(10, 75, 300, 250)];
    self.mainView.layer.cornerRadius = 5;
    self.mainView.layer.masksToBounds =YES;
    [self.mainView setBackgroundColor:[UIColor whiteColor]];
    
    [self.mainView addSubview:self.quoteField];
    [self.mainView addSubview:self.authorField];
    [self.mainView addSubview:self.titleField];
    
    [self.view addSubview:self.mainView];
    
    [self.view setBackgroundColor:[UIColor grayColor]];
}


- (void)cancelButtonPressed
{
    [self.delegate didDismissAddView];
}

- (void)saveButtonPressed
{
    NSDictionary *tempQuote = @{@"quote":self.quoteField.text, @"author":self.authorField.text, @"title":self.titleField.text};
//    Quote* quote = [[Quote alloc] initWithQuote:self.quoteField.text andAuthor:self.authorField.text andTitle:self.titleField.text];
    [self.delegate didDismissAddViewWithQuote:tempQuote];
}

- (CGSize)text:(NSString *)text sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size
{
    if ([text respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)])
    {
        CGRect frame = [text boundingRectWithSize:size
                                          options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                       attributes:@{NSFontAttributeName:font}
                                          context:nil];
        return frame.size;
    }
    else
    {
        return [text sizeWithFont:font constrainedToSize:size];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
