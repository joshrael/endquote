//
//  Database.h
//  EndQuote
//
//  Created by Josh Rael on 4/19/14.
//  Copyright (c) 2014 Josh Rael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Quote.h"
#import <sqlite3.h>

@interface Database : NSObject


+(void) createEditableCopyOfDatabaseIfNeeded;
+(void) initDatabase;
+(NSMutableArray*) fetchAllQuotes;
+(NSMutableArray*) fetchAllAuthors;
+(NSMutableArray*) fetchAllQuotesByAuthor:(NSString*)author;
+(NSMutableArray*) fetchAllTitles;
+(NSMutableArray*) fetchAllQuotesByTitle:(NSString*)title;
+(void) saveQuoteWithQuote:(NSString*)quote andAuthor:(NSString*)author andTitle:(NSString*)title;
+(void) deleteQuote:(int)rowid;
+(void) cleanUpDatabaseForQuit;
 

@end
